/**
* UrlInfo handler
*/
function handleCustomBatchInfo(){
  var templateName = 'CustomBatchInfo';
  var templateData = {

  };

  loadTemplate(templateName, function(data){
    source = data;
    template = Handlebars.compile(source);
    $dynamicSpaContainer.html(template(templateData));

    var $domainsList = $('#domains-list');
    var $submitButton = $("#submit-button");
    var $fetchedCounter = $("#fetched-counter");
    var $saveAsPdf = $("#save-as-pdf");

    var $responseContainer = $("#response-container");
    var $responseDataContainer = $("#response-data-container");
    var $responseToggleButtonsContainer = $("#response-toggle-buttons-container");

    //$domainsList.val("google.com\ngmail.com\ngithub.com\nflipkart.com");

    $saveAsPdf.click(function(){
        var specialElementHandlers = {
            '#editor': function (element, renderer) {
                return true;
            }
        };

        if(jsPdf) {
            // var res = jsPdf.autoTableHtmlToJson($responseDataContainer.find("table:eq(0)").get(0));
            // jsPdf.autoTable(res.columns, res.data, {
            //     startY: 60,
            //     tableWidth: 'auto',
            //     columnWidth: 'auto',
            //     styles: {
            //       overflow: 'linebreak'
            //     }
            // });
            jsPdf.fromHTML($responseDataContainer.find("table:eq(0)").get(0), 15, 15, {
                'width': 170, 'elementHandlers': specialElementHandlers
            });
            jsPdf.save('domain_details.pdf');
        } else {
            cl("jsPdf null");
        }
    });

    loadTemplateWithBase('TableTemplate', 'custom_batch/', function(data){
      var tableTemplate = data;
      var compiledTableTemplate = Handlebars.compile(tableTemplate);
      $responseDataContainer.append(compiledTableTemplate());

      var $resultsTable = $("#results-table");

      loadTemplateWithBase('RowTemplate', 'custom_batch/', function(data){
          var rowTemplate = data;
          var compiledRowTemplate = Handlebars.compile(rowTemplate);

          $submitButton.click(function(){
            var domains = $domainsList.val().split("\n");

            var fetchNextDomainInfo = function(domainIndex){
              if(domainIndex < (domains.length)) {
                  cl("fetching: " + domains[domainIndex]);
                var requestData = {
                    'domain_name': domains[domainIndex]
                };

                getData(templateName, requestData, function(data) {
                    var addressBase = data.contactInfo.physicalAddress;
                    var categoryPath = "";
                    var physicalAddress = [];

                    if(data.related && data.related.categories && data.related.categories.categoryData) {
                        if(Object.prototype.toString.call(data.related.categories.categoryData) == '[object Array]') {
                            categoryPath = data.related.categories.categoryData[0].absolutePath;
                        } else {
                            categoryPath = data.related.categories.categoryData.absolutePath;
                        }
                    }

                    if(addressBase.streets && addressBase.streets.street) {
                        physicalAddress.push(addressBase.streets.street);
                    }

                    if(addressBase.city) {
                        physicalAddress.push(addressBase.city);
                    }

                    if(addressBase.state) {
                        physicalAddress.push(addressBase.state);
                    }

                    if(addressBase.country) {
                        physicalAddress.push(addressBase.country);
                    }

                    if(addressBase.postalCode) {
                        physicalAddress.push(addressBase.postalCode);
                    }

                    var rowData = {
                        'td1': requestData.domain_name,
                        'td2': categoryPath,
                        'td3': physicalAddress.join(', ')
                    };
                    $resultsTable.append(compiledRowTemplate(rowData));
                    $fetchedCounter.text('Fetched ' + (domainIndex + 1) + ' of ' + domains.length);

                  fetchNextDomainInfo(++domainIndex);
                },function(xhr, textStatus, errorThrown) {
                  cl("xhr, textStatus, errorThrown: " + xhr + ", " + textStatus + ", " + errorThrown);
                });
              } else {
                $.notify("All domains processed", {position: "top"}, "info");
              }
            };

            fetchNextDomainInfo(0);
          });
      });
    });
  });
}



/**
* SitesLinkingIn handler
*/
function handleSitesLinkingIn(){
  var templateData = {
    "default_count": 20,
    "default_start_index": 0
  };

  alexaUIHelper('SitesLinkingIn', templateData, function(templateData){

  }, function(){
    var $companyDomain = $("#company_domain");
    var $count = $("#count");
    var $startIndex = $("#start_index");

    if($companyDomain.val().length < 2) {
      $companyDomain.notify("Please enter a valid domain", {position: "bottom"}, "error");
      return null;
    }

    try {
      var n = parseInt($count.val());
      if(isNormalInteger($count.val()) == false) {
          $count.notify("Please enter a number greater than 0 and less than 21", {position: "bottom"}, "error");
          return null;
      }
    } catch(err){
      $count.notify("Please enter a valid number greater than 0 and less than 21", {position: "bottom"}, "error");
      return null;
    }

    try {
      var n = parseInt($startIndex.val());
      if(n < 0) {
        $startIndex.notify("Please enter a valid number greater than 0", {position: "bottom"}, "error");
        return null;
      }
    } catch(err){
      $startIndex.notify("Please enter a valid number greater than 0", {position: "bottom"}, "error");
      return null;
    }

    return {
      'domain_name': $companyDomain.val(),
      "count": $count.val(),
      "start": $startIndex.val()
    };
  });
}

/**
* CategoryListings handler
*/
function handleCategoryListings(){
  var templateData = {
    "vals": "Popularity,Title,AverageReview".split(","),
    "default_path": "Top/Business/Automotive",
    "default_descriptions": true,
    "default_recursive": true,
    "default_count": 20,
    "default_start_index": 1
  };

  alexaUIHelper('CategoryListings', templateData, function(templateData){
    var $sortBySelect = $('#sort-by');
    var $descriptionsSelect = $('#descriptions');
    var $recursiveSelect = $('#recursive');

    $sortBySelect.material_select();
    $descriptionsSelect.material_select();
    $recursiveSelect.material_select();
  }, function(){
    var $sortBySelect = $('#sort-by');
    var $descriptionsSelect = $('#descriptions');
    var $recursiveSelect = $('#recursive');

    var $path = $("#path");
    var $count = $("#count");
    var $startIndex = $("#start_index");

    if($sortBySelect.val() === null || $sortBySelect.val().length < 1) {
        $sortBySelect.notify("Please select sort-by value", {position: "top"}, "error");
        return null;
    }

    if($descriptionsSelect.val() === null || $descriptionsSelect.val().length < 1) {
        $descriptionsSelect.notify("Please select descriptions value", {position: "top"}, "error");
        return null;
    }

    if($recursiveSelect.val() === null || $recursiveSelect.val().length < 1) {
        $recursiveSelect.notify("Please select recursive value", {position: "top"}, "error");
        return null;
    }

    if($path.val().length < 1) {
      $path.notify("Please enter a valid path", {position: "bottom"}, "error");
      return null;
    }

    try {
      var n = parseInt($count.val());
      if(isNormalInteger($count.val()) == false) {
          $count.notify("Please enter a number greater than 0 and less than 21", {position: "bottom"}, "error");
          return null;
      }
    } catch(err){
      $count.notify("Please enter a valid number greater than 0 and less than 21", {position: "bottom"}, "error");
      return null;
    }

    try {
      var n = parseInt($startIndex.val());
      if(n < 1) {
        $startIndex.notify("Please enter a valid number greater than or equal to 1", {position: "bottom"}, "error");
        return null;
      }
    } catch(err){
      $startIndex.notify("Please enter a valid number greater than or equal to 0", {position: "bottom"}, "error");
      return null;
    }

    return {
      'path': $path.val(),
      'descriptions': $descriptionsSelect.val(),
      'sortBy': $sortBySelect.val(),
      "recursive": $recursiveSelect.val(),
      "count": $count.val(),
      "start": $startIndex.val()
    };
  });
}

/**
* CategoryBrowse handler
*/
function handleCategoryBrowse(){
  var templateData = {
    "vals": "Categories,RelatedCategories,LanguageCategories,LetterBars".split(",")
  };

  alexaUIHelper('CategoryBrowse', templateData, function(templateData){
    var $selectElement = $('#response-groups');
    var $descriptionsSelect = $('#descriptions');

    $selectElement.material_select();
    $descriptionsSelect.material_select();
  }, function(){
    var $selectElement = $('#response-groups');
    var $descriptionsSelect = $('#descriptions');
    var $path = $("#path");

    if($descriptionsSelect.val() === null || $descriptionsSelect.val().length < 1) {
        $descriptionsSelect.notify("Please select descriptions value", {position: "top"}, "error");
        return null;
    }

    return {
      'path': $path.val(),
      'descriptions': $descriptionsSelect.val(),
      'response_groups': $selectElement.val()
    };
  });
}

/**
* TrafficHistory handler
*/
function handleTrafficHistory(){
  var templateData = {
    "default_days": 31,
    "default_start_data": "20151001"
  };

  alexaUIHelper('TrafficHistory', templateData, function(templateData){

  }, function(){
    var $companyDomain = $("#company_domain");
    var $daysRange = $("#days_range");
    var $startDate = $("#start_date");

    if($companyDomain.val().length < 2) {
      $companyDomain.notify("Please enter a valid domain name", {position: "bottom"}, "error");
      return null;
    }

    if(isNormalInteger($daysRange.val()) == false) {
        $daysRange.notify("Please enter a number greater than 0 and less than 32", {position: "bottom"}, "error");
        return null;
    }

    return {
      'domain_name': $companyDomain.val(),
      'range': $daysRange.val(),
      'start': $startDate.val(),
    };
  });
}

/**
* UrlInfo handler
*/
function handleUrlInfo(){
  var templateData = {
    "vals": "RelatedLinks,Categories,Rank,RankByCountry,UsageStats,ContactInfo,AdultContent,Speed,Language,OwnedDomains,LinksInCount,SiteData,Related,TrafficData,ContentData".split(",")
  };

  alexaUIHelper('UrlInfo', templateData, function(templateData){
    var $selectElement = $('#response-groups');

    $selectElement.material_select();
    $selectElement.change(function(){});
  }, function(){
    var $selectElement = $('#response-groups');
    var $companyDomain = $("#company_domain");

    if($companyDomain.val().length < 2) {
      $companyDomain.notify("Please enter a valid domain", {position: "bottom"}, "error");
      return null;
    }

    if($selectElement.val() == null || $selectElement.val().length < 1) {
        $selectElement.notify("Please select a response group", {position: "bottom"}, "error");
        return null;
    }

    return {
      'domain_name': $companyDomain.val(),
      'response_groups': $selectElement.val()
    };
  });
}
